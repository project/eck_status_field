<?php

namespace Drupal\eck_status_field\Entity;

use Drupal\eck\Entity\EckEntityType;

/**
 * Entity type class for ECK entities with a status/published base field.
 */
class PublishedEckEntityType extends EckEntityType {

  /**
   * If this entity type has a status/published base field.
   *
   * @var bool
   */
  protected $published;

  /**
   * Whether this ECK entity type has a status/published base field.
   */
  public function hasPublishedField(): bool {
    return isset($this->published) && $this->published;
  }

}
