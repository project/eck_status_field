<?php

namespace Drupal\eck_status_field\Entity;

use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\eck\Entity\EckEntity;

/**
 * Entity class for ECK entities with a status field.
 */
class PublishedEckEntity extends EckEntity implements EntityPublishedInterface {

  use EntityPublishedTrait;

}
