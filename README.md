# ECK Status Field

This module adds support for publishing Entity Construction Kit
(ECK) entities by adding a status base field to entity types.
To enable the status field, visit the ECK entity type edit page
and toggle the Published field checkbox.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/eck_status_field).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/eck_status_field).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

Drupal 8.8 or later and the Entity Construction Kit (ECK) module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Install and enable the Entity Construction Kit (ECK) module.
   [Entity Construction Kit](https://www.drupal.org/project/eck)
2. Create a new ECK entity type and toggle the Published field checkbox.


## Maintainers

- Dieter Holvoet - [DieterHolvoet](https://www.drupal.org/u/dieterholvoet)
